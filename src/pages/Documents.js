import React from 'react'
import PdfViewer from './PdfViewer'


class Documents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {user_id: ''};
}
  render() {
    return (
        <div className='header-text col-6 center'>
          <h1>Documents</h1>
          <div id="react-container"><PdfViewer /></div>
        </div>
    );
  }
}

export default Documents;
