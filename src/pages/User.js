import React from 'react'
import UserForm from './UserForm'
import './pages.css'


class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {user_id: ''};
  }

  render() {
    return (
      <div className='header-text col-6 center'>
          <h1>User Search</h1>
          <UserForm />
      </div>
    )
  }
}

export default User;
