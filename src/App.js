import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import Documents from './pages/Documents';
import User from './pages/User';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/user' component={User} />
          <Route path='/documents' component={Documents} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
