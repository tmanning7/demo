import React from 'react';
import './pages.css';
import {Form, Button, Table} from 'react-bootstrap';
import * as FaIcons from 'react-icons/fa';
import { UserData } from './UserData';

class UserForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: '',user_id: '',is_submitted: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log(this.state)
    this.setState({value: event.target.value});
  }

// Handles how the inputted value is handled

  handleSubmit(event) {
    event.preventDefault();

    this.setState({
        user_id: this.state.value,
        is_submitted: true
    });

    //Clear form input after successful submission
    var form = document.getElementById('user_form');
    form.reset();
  }

  render() {
      if (this.state.is_submitted) {
        if (this.state.user_id in UserData) {
          const data = UserData[this.state.user_id]
          console.log(data)
          return (
            <>
            <Form id='user_form' onSubmit={this.handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Control type="text"
                              placeholder="Type a Username to Start..."
                              className="text-center"
                              onChange={this.handleChange} />
              </Form.Group>
              <Button variant="primary" type="submit">
                <FaIcons.FaSearch />
              </Button>
            </Form>
            <hr />
              <Table striped bordered hover responsive size="sm" variant="dark">
                <thead>
                  <tr>
                    <th>User ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{this.state.user_id}</td>
                      <td>{data.FirstName}</td>
                      <td>{data.LastName}</td>
                      <td>{data.Email}</td>
                    </tr>
                  </tbody>
              </Table>
            </>
          );
        }
        else {
          return (
            <>
              <Form id='user_form' onSubmit={this.handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Control type="text"
                                placeholder="Type a Username to Start..."
                                className="text-center"
                                onChange={this.handleChange} />
                </Form.Group>
                <Button variant="primary" type="submit">
                  <FaIcons.FaSearch />
                </Button>
              </Form>
              <hr />
              <h3>User Not Found... Try Typing something else</h3>
            </>
          );
        }
      }
      else {
        return (
          <>
          <Form id='user_form' onSubmit={this.handleSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Control type="text"
                            placeholder="Type a Username to Start..."
                            className="text-center"
                            onChange={this.handleChange} />
            </Form.Group>
            <Button variant="primary" type="submit">
              <FaIcons.FaSearch />
            </Button>
          </Form>
          </>
      );
    }
  }
}

export default UserForm;
